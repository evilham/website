website: howtos pkg-sets
	pandoc --template template.html --css /style.css \
		-f markdown -t html < index.md > index.html
	tidy -quiet -mi --indent-spaces 4 --wrap 111 index.html

howtos:
.for f in index bootstrap jails fresh howdo abi-changes
	pandoc --template template.html --css /style.css \
		-f markdown -t html < howto/${f}.md > howto/${f}.html
	tidy -quiet -mi --indent-spaces 4 --wrap 111 howto/${f}.html
.endfor

pkg-sets:
.for f in current stable release
	pandoc --template template.html --css /style.css \
		-f markdown -t html < ${f}/index.md > ${f}/index.html
	tidy -quiet -mi --indent-spaces 4 --wrap 111 ${f}/index.html
.endfor

update-static: website
	mkdir -p build/howto build/current build/stable build/release
	cp alpha.pkgbase.live.pub build/
	cp favicon.png build/
	cp style.css build/
	mv index.html build/
	mv howto/*.html build/howto/
	mv current/index.html build/current/
	mv stable/index.html build/stable/
	mv release/index.html build/release/
	mkdir -p site/
	cp -r build/* site/
	rm -r build
